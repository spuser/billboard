/**
 * Test modules and server
 *
 * @author Palicka
 *
 **/

var assert = require("assert"); // core module
var C = require('./mycount');  // my module
var F = require('./myfile');  // my module
var request = require('./node_modules/supertest');
var express = require('express');
var track = require('./track');

var app = express();
app.use('/track', track);

describe('TEST MODULES', function () {
    describe('exist modules: ', function () {
        it('Module C - should have a addCount method', function () {
            assert.equal(typeof C, 'object');
            assert.equal(typeof C.addCount, 'function');

        });
        it('Module F - should have a appendJson method', function () {
            assert.equal(typeof F, 'object');
            assert.equal(typeof F.appendJson, 'function');

        });
    });
    describe('mycount: module C ', function () {
        beforeEach(function (done) {
            C.setCount(0, done);
        });
        it('addCount() should equal 1', function (done) {
            // test -> count + 1
            C.addCount(function (err, data) {
                if (err)
                    return done(err);
                assert.equal(data, 1);
                done();
            });
        });
        after(function (done) {
            C.setCount(0, done);
        });
    });
    describe('myfile: module F', function () {
        beforeEach(function (done) {
            F.delJson(done);
        });
        it('append no query to JSON - not exists file', function (cb) {
            var json = {};
            F.appendJson(json, function (err) {
                if (err)
                    return cb(err);
                F.readJson(function (err, data) {
                    if (err)
                        return done(err);
                    assert.deepEqual(data, [{}]);
                    cb();
                });
            });
        });
        it('append query to JSON - not exists file', function (cb) {
            var json = {"test": "test"};
            F.appendJson(json, function (err) {
                if (err)
                    return cb(err);
                F.readJson(function (err, data) {
                    if (err)
                        return done(err);
                    assert.deepEqual(data, [{"test": "test"}]);
                    cb();
                });
            });
        });
        after(function (done) {
            F.delJson(done);
        });
    });
    describe('myfile: module F', function () {
        beforeEach(function (done) {
            F.initTest(done);
        });
        it('append no query to JSON - with exists file', function (cb) {
            var json = {};
            F.appendJson(json, function (err) {
                if (err)
                    return cb(err);
                F.readJson(function (err, data) {
                    if (err)
                        return done(err);
                    assert.deepEqual(data, [{}]);
                    cb();
                });
            });
        });
        it('append query to JSON - with exists file', function (cb) {
            var json = {"test": "test"};
            F.appendJson(json, function (err) {
                if (err)
                    return cb(err);
                F.readJson(function (err, data) {
                    if (err)
                        return done(err);
                    assert.deepEqual(data, [{"test": "test"}]);
                    cb();
                });
            });
        });
        it('multi append query to JSON - with exists file', function (cb) {
            var json = {"test": "test"};
            F.appendJson(json, function (err) {
                if (err)
                    return cb(err);
                F.appendJson(json, function (err) {
                    if (err)
                        return cb(err);
                    F.readJson(function (err, data) {
                        if (err)
                            return cb(err);
                        assert.deepEqual(data, [{"test": "test"}, {"test": "test"}]);
                        cb();
                    });
                });
            });
        });
        after(function (done) {
            F.delJson(done);
        });
    });
});

describe('TEST API', function () {
    describe('GET /track', function () {
        it('only status 200', function (done) {
            request(app)
                    .get('/track')
                    .expect(200)
                    .end(function (err, res) {
                        if (err)
                            return done(err);
                        done();
                    });
        });
        after(function (done) {
            F.delJson(done);
        });
    });
    describe('GET /track?test=1', function () {
        beforeEach(function (done) {
            F.initTest(done);
        });
        it('status 200 and append query to json file', function (done) {
            request(app)
                    .get('/track')
                    .query({test: "1"})
                    .expect(200)
                    .end(function (err, res) {
                        if (err)
                            return done(err);
                        F.readJson(function (err, data) {
                            //console.log('res:' + data);
                            if (err)
                                return done(err);
                            assert.deepEqual(data, [{"test": "1"}]);
                            done();
                        });
                    });
        });
        after(function (done) {
            F.delJson(done);
        });
    });
    describe('GET /track?test=1&count=1', function () {
        beforeEach(function (done) {
            C.setCount(0, done);
        });
        it('status 200, append query to json file and +1 to db', function (done) {
            request(app)
                    .get('/track')
                    .query({test: "1", count: "1"})
                    .expect(200)
                    .end(function (err, res) {
                        if (err)
                            return done(err);
                        C.getCount(function (err, data) {
                            if (err)
                                return done(err);
                            assert.equal(data, 1);
                            F.readJson(function (err, data) {
                                if (err)
                                    return done(err);
                                assert.deepEqual(data, [{"test": "1", "count": "1"}]);
                                done();
                            });
                        });
                    });
        });
    });
});