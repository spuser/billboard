/**
 * Module for storage in the database
 *
 * @author Palicka
 *
 **/

var client = require('redis').createClient();

var key = 'count';
var C = {};

/**
 * Add count +1 -> DB
 **/
C.addCount = function (cb) {
    'use strict';
    client.on('error', function (err) {
        console.log('Error ' + err);
    });
    client.exists(key, function (err, reply) {
        if (err)
            return cb(err, null);
        if (reply) {
            client.incr(key, function (err, data) {
                if (err)
                    return cb(err, null);
                cb(err, data);
            });
        } else {
            client.set(key, 0, function (err) {
                if (err)
                    return cb(err, null);
                client.incr(key, function (err, data) {
                    if (err)
                        return cb(err, null);
                    cb(err, data);
                });
            });
        }
    });
};

/**
 * Set count
 **/
C.setCount = function (count, cb) {
    client.set(key, count, function (err) {
        if (err)
            return cb(err);
        cb();
    });
};

/**
 * Get count
 **/
C.getCount = function (cb) {
    client.get(key, function (err, data) {
        if (err)
            return cb(err, null);
        cb(null, data);
    });
};
module.exports = C;           