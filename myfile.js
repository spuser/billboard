/**
 * Module for handling JSON file
 *
 * @author Palicka
 *
 **/

var fs = require('fs');

var file = './file.json';
var F = {};

/*
 * Append json to file
 */
F.appendJson = function (query, cb) {//next
    fs.exists(file, function (exists) {
        //console.log('query::' + query);
        if (exists) {
            fs.readFile(file, function (err, data) {
                //console.log('err::' + err);
                //console.log('data::' + data);
                if (err)
                    return cb(err);
                saveJsonFile(addQuery(JSON.parse(data), query), function () {
                    if (err)
                        return cb(err);
                    cb();
                });
            });
        } else {
            var content = [];
            saveJsonFile(addQuery(content, query), function (err) {
                if (err)
                    return cb(err);
                cb();
            });
        }
    });
};

/**
 * Read json file
 **/
F.readJson = function (cb) {
    fs.readFile(file, function (err, data) {
        //console.log('err:::' + err);
        if (err)
            return cb(err, null);
        //console.log('data:::' + data);
        var jsdata = JSON.parse(data);
        cb(null, jsdata);
    });
};

/**
 * Init json file with []
 **/
F.initTest = function (cb) {
    fs.writeFile(file, JSON.stringify([]), function (err) {
        if (err)
            return cb(err);
        cb();
    });
};
/**
 * Delete json file
 */
F.delJson = function (cb) {
    fs.exists(file, function (exists) {
        if (exists) {
            fs.unlink(file, function (err) {
                if (err)
                    return cb(err);
                cb();
            });
        }
        else {
            cb();
        }

    });
};

/*
 * Save file - file.json
 * @param {type} content
 */
function saveJsonFile(content, cb) {
    //console.log('It\'s saved!-content' + JSON.stringify(content));
    fs.writeFile(file, JSON.stringify(content), function (err) {
        //console.log('It\'s saved!-err:' + err);
        if (err)
            return cb(err);

        //console.log('It\'s saved!');
        cb();
    });
}

/*
 * Add query parameters
 * @param {type} content
 * @returns {unresolved}
 */
function addQuery(content, query) {
    if (Array.isArray(content)) {
        //console.log('data:array:content:' + content);
        if (query === null) {
        } else {
            content.push(query);
        }
        //console.log('data:array:' + JSON.stringify(content));
    }
    return content;
}

module.exports = F;



