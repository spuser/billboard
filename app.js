/**
 * Server initialization
 *
 * @author Palicka
 *
 **/

var express = require('express');
var track = require('./track');

var app = express();

app.use('/track', track);

module.exports = app;

var host = "127.0.0.1";
var port = 3000;

app.listen(port, host);