/**
 * Track - route
 *
 * @author Palicka
 *
 **/
var express = require('express');
var router = express.Router();
var C = require('./mycount');  // my module
var F = require('./myfile');  // my module

/**
 *  GET /track?count=1&test=1 listing.
 */
router.get('/', function (req, res, next) {
    if (req.query) {
        F.appendJson(req.query, function (err) {
            if (err)
                res.sendStatus(400);
            if (req.query.count) {
                C.addCount(function (err) {
                    if (err)
                        res.sendStatus(400);
                    res.sendStatus(200);
                });
            } else {
                res.sendStatus(200);
            }
        });
    } else {
        res.sendStatus(200);
    }
});

module.exports = router;


